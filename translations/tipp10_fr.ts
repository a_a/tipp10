<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>AbcRainWidget</name>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="68"/>
        <source>Press space bar to start</source>
        <translation>Appuyez sur la touche Espace pour commencer</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="93"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="95"/>
        <source>&amp;Pause</source>
        <translation>&amp;Pause</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="98"/>
        <source>E&amp;xit Game</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="338"/>
        <source>Number of points:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="402"/>
        <source>Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="403"/>
        <source>Points:</source>
        <translation>Points:</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="414"/>
        <source>Press space bar to proceed</source>
        <translation>Tapez Espace pour démarrer</translation>
    </message>
</context>
<context>
    <name>CharSqlModel</name>
    <message>
        <location filename="../sql/chartablesql.cpp" line="45"/>
        <source>%L1 %</source>
        <translation>%L1 %</translation>
    </message>
</context>
<context>
    <name>CharTableSql</name>
    <message>
        <location filename="../sql/chartablesql.cpp" line="73"/>
        <source>Characters</source>
        <translation>Caractères</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="74"/>
        <source>Target Errors</source>
        <translation>Erreurs</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="75"/>
        <source>Actual Errors</source>
        <translation>Typo</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="76"/>
        <source>Frequency</source>
        <translation>Fréquence</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="77"/>
        <source>Error Rate</source>
        <translation>Taux d&apos;erreur</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="80"/>
        <source>This column shows all of the
characters typed</source>
        <translation>Cette colonne affiche tous les caractères tapés</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="84"/>
        <source>The character was supposed to be typed, but wasn&apos;t</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="87"/>
        <source>Character was mistyped</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="89"/>
        <source>This column indicates the total frequency of each
character shown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="93"/>
        <source>The error rate shows which characters give
you the most problems. The error rate is
calculated from the value &quot;Target Error&quot;
and the value &quot;Frequency&quot;.</source>
        <translation>Le taux d&apos;erreurs indique les caractères
qui vous posent le plus de problèmes.
C&apos;est le rapport entre le nombre d&apos;erreurs et
la fréquence d&apos;apparition du caractère dans la dictée.</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="117"/>
        <source>Reset characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="191"/>
        <source>Recorded error rates affect the intelligence feature and the selection of the text to be dictated. If the error rate for a certain character is excessively high it might be useful to reset the list.

All recorded characters will now be deleted.

Do you still wish to proceed?
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ErrorMessage</name>
    <message>
        <location filename="../widget/errormessage.cpp" line="68"/>
        <source>The process will be aborted.</source>
        <translation>L&apos;opération sera annulée.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="71"/>
        <source>The update will be aborted.</source>
        <translation>La mise à jour sera annulée.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="74"/>
        <source>The program will be aborted.</source>
        <translation>Le programme sera arrêté.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="85"/>
        <source>Cannot load the program logo.</source>
        <translation>Erreur au chargement du logo.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="88"/>
        <source>Cannot load the keyboard bitmap.</source>
        <translation>Impossible de charger l&apos;image du clavier.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="91"/>
        <source>Cannot load the timer background.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="94"/>
        <source>Cannot load the status bar background.</source>
        <translation>Impossible de charger le fond de l&apos;image de la barre de statut.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="99"/>
        <source>Cannot find the database %1. The file could not be imported.
Please check whether it is a readable text file.</source>
        <translation>Impossible de trouver la base de données %1. Importation du fichier impossible.
Veuillez vérifier qu&apos;il s&apos;agit bien d&apos;un fichier texte.</translation>
    </message>
    <message>
        <source>Cannot find the database in the specified directory.
TIPP10 is trying to create a new, empty database in the specified directory.

You can change the path to the database in the program settings.
</source>
        <translation type="vanished">Impossible de trouver la base de données dans le répertoire indiqué.
TIPP10 va créer une nouvelle base de données dans ce répertoire.

Vous pouvez changer le chemin vers la base de données dans les réglages du programme.
</translation>
    </message>
    <message>
        <source>Cannot create the user database in your HOME directory. There may be no permission to write.
TIPP10 is trying to use the original database in the program directory.

You can change the path to the database in the program settings later.
</source>
        <translation type="vanished">Impossible de créer la base de données dans votre répertoire personnel. Vous n&apos;avez peut-être pas les droits d&apos;écriture.
TIPP10 va utiliser la base de données originale placée dans le répertoire du programme.

Vous pourrez changer son emplacement dans les réglages du programme.
</translation>
    </message>
    <message>
        <source>Cannot create the user database in the specified directory. There may be no directory or no permission to write.
TIPP10 is trying to create a new, empty database in your HOME directory.

You can change the path to the database in the program settings later.
</source>
        <translation type="vanished">Impossible de créer la base de données à l&apos;emplacement indiqué. Le répertoire n&apos;existe pas ou vous n&apos;avez pas les droits d&apos;écriture.
TIPP10 va tenter de la créer dans votre répertoire personnel.

Vous pourrez changer son emplacement dans les réglages du programme.
</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="121"/>
        <location filename="../widget/errormessage.cpp" line="125"/>
        <source>Connection to the database failed.</source>
        <translation>La connexion à la base de données a échoué.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="128"/>
        <source>The user table with lesson data  cannot be emptied.
SQL statement failed.</source>
        <translation>La table des leçons personnelles n&apos;a pu être vidée.
La requête SQL a échouée.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="132"/>
        <source>The user table with error data cannot be emptied.
SQL statement failed.</source>
        <translation>La table des erreurs n&apos;a pu être vidée.
La requête SQL a échouée.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="136"/>
        <source>No lessons exist.</source>
        <translation>Il n&apos;y a pas de leçon.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="139"/>
        <source>No lesson selected.
Please select a lesson.</source>
        <translation>Veuillez sélectionner une leçon.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="142"/>
        <source>Cannot create the lesson.
SQL statement failed.</source>
        <translation>Impossible de créer la leçon.
La requête SQL a échoué.</translation>
    </message>
    <message>
        <source>The lesson could not be updated because there is no
access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file and restart the software (it
will create a new, empty database automatically).
You can find the database path &quot;%1&quot; in the General Settings.

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</source>
        <translation type="vanished">Les données de la leçon personnelle n&apos;ont pu être sauvegardées
dans la base de données.

Si ce problème apparaît après avoir noté un ralentissement de votre ordinateur,
il se peut que la base de données ait été endommagée (par un crash de l&apos;ordinateur par ex.).
Pour vérifier cela, vous pouvez renommer le fichier contenant la base de données et 
redémarrer le logiciel (il créera automatiquement une nouvelle base de données).

Vous trouverez le chemin de la base de données &quot;%1&quot; dans les réglages.

Si ce problème se produit au premier démarrage de l&apos;application,
vérifiez que la base de données est bien accessible en lecture et en écriture.</translation>
    </message>
    <message>
        <source>The user typing errors could not be updated because
there is no access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file and restart the software (it
will create a new, empty database automatically).
You can find the database path &quot;%1&quot; in the General Settings.

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</source>
        <translation type="vanished">Les statistiques d&apos;erreurs de frappe n&apos;ont pu être sauvegardées
dans la base de données.

Si ce problème apparaît après avoir noté un ralentissement de votre ordinateur,
il se peut que la base de données ait été endommagée (par un crash de l&apos;ordinateur par ex.).
Pour vérifier cela, vous pouvez renommer le fichier contenant la base de données et 
redémarrer le logiciel (il créera automatiquement une nouvelle base de données).

Vous trouverez le chemin de la base de données &quot;%1&quot; dans les réglages.

Si ce problème se produit au premier démarrage de l&apos;application,
vérifiez que la base de données est bien accessible en lecture et en écriture.</translation>
    </message>
    <message>
        <source>The user lesson data could not be updated because
there is no access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file and restart the software (it
will create a new, empty database automatically).
You can find the database path &quot;%1&quot; in the General Settings.

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</source>
        <translation type="vanished">Les données de la leçon personnelle n&apos;ont pu être sauvegardées
dans la base de données.

Si ce problème apparaît après avoir noté un ralentissement de votre ordinateur,
il se peut que la base de données ait été endommagée (par un crash de l&apos;ordinateur par ex.).
Pour vérifier cela, vous pouvez renommer le fichier contenant la base de données et 
redémarrer le logiciel (il créera automatiquement une nouvelle base de données).

Vous trouverez le chemin de la base de données &quot;%1&quot; dans les réglages.

Si ce problème se produit au premier démarrage de l&apos;application,
vérifiez que la base de données est bien accessible en lecture et en écriture.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="104"/>
        <source>Cannot find the database in the specified directory.
TIPP10 is trying to create a new, empty database in the specified directory.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="109"/>
        <source>Cannot create the user database in your AppData directory. There may be no permission to write.
TIPP10 is trying to use the original database in the program directory.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="115"/>
        <source>Cannot create the user database in the specified directory. There may be no directory or no permission to write.
TIPP10 is trying to create a new, empty database in your AppData directory.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="146"/>
        <source>The lesson could not be updated because there is no
access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file &quot;%1&quot; and restart the software (it
will create a new, empty database automatically).

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="160"/>
        <source>The user typing errors could not be updated because
there is no access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file &quot;%1&quot; and restart the software (it
will create a new, empty database automatically).

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="174"/>
        <source>The user lesson data could not be updated because
there is no access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file &quot;%1&quot; and restart the software (it
will create a new, empty database automatically).

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="187"/>
        <source>Cannot save the lesson.
SQL statement failed.</source>
        <translation>Impossible de sauvegarder les données de la leçon personnelle.
La requête SQL a échouée.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="190"/>
        <source>Cannot retrieve the lesson.
SQL statement failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="193"/>
        <source>Cannot analyze the lesson.
SQL statement failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="196"/>
        <source>The file could not be imported.
Please check whether it is a readable text file.
</source>
        <translation>Importation du fichier impossible.
Veuillez vérifier qu&apos;il s&apos;agit bien d&apos;un fichier texte.
</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="201"/>
        <source>The file could not be imported because it is empty.
Please check whether it is a readable text file with content.
</source>
        <translation>Importation du fichier impossible : il est vide.
Veuillez indiquer un fichier texte non vide.
</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="206"/>
        <source>The file could not be imported.
Please check the spelling of the web address;
it must be a valid URL and a readable text file.
Please also check your internet connection.</source>
        <translation>Importation du fichier impossible.
Vérifiez que l&apos;adresse Internet indiquée pointe bien vers un fichier texte.
Veuillez vérifier aussi votre connexion Internet.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="211"/>
        <source>The file could not be exported.
Please check to see whether it is a writable text file.
</source>
        <translation>Impossible d&apos;exporter le fichier.
Veuillez vérifier qu&apos;il s&apos;agit bien d&apos;un fichier texte modifiable.
</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="215"/>
        <source>Cannot create temporary file.</source>
        <translation>Impossible de créer de fichier temporaire.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="218"/>
        <source>Cannot execute the update process.
Please check your internet connection and proxy settings.</source>
        <translation>Impossible d&apos;effectuer la mise à jour.
Vérifiez votre connexion Internet et les paramètres de votre proxy si vous en utilisez un.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="222"/>
        <source>Cannot read the online update version.</source>
        <translation>Impossible de lire la version du programme sur Internet.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="225"/>
        <source>Cannot read the database update version.</source>
        <translation>Impossible de lire la version de la base de données.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="228"/>
        <source>Cannot execute the SQL statement.</source>
        <translation>Impossible d&apos;exécuter la requête SQL.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="231"/>
        <source>Cannot find typing mistake definitions.</source>
        <translation>Aucune définition d&apos;erreur de saisie n&apos;a été trouvé.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="234"/>
        <source>Cannot create temporary file.
Update failed.</source>
        <translation>Impossible de créer de fichier temporaire.
La mise à jour a échoué.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="237"/>
        <source>Cannot create analysis table.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="240"/>
        <source>Cannot create analysis index.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="243"/>
        <source>Cannot fill analysis table with values.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="247"/>
        <source>An error has occured.</source>
        <translation>Une erreur s&apos;est produite.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="251"/>
        <source>
(Error number: %1)
</source>
        <translation>
(Erreur numéro : %1)
</translation>
    </message>
</context>
<context>
    <name>EvaluationWidget</name>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="43"/>
        <source>Report</source>
        <translation>Rapport</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="47"/>
        <source>Overview of Lessons</source>
        <translation>Leçons effectuées</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="48"/>
        <source>Progress of Lessons</source>
        <translation>Progression</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="49"/>
        <source>Characters</source>
        <translation>Caractères</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="50"/>
        <source>Fingers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="51"/>
        <source>Comparison Table</source>
        <translation>Table de comparaison</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="72"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="73"/>
        <source>&amp;Close</source>
        <translation>&amp;Fermer</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="143"/>
        <source>Use examples</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="147"/>
        <source>Please note that you get better scores for slow typing without errors, than for fast typing with lots of errors!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="157"/>
        <source>Score</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="162"/>
        <source>For example, this equates to …</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="167"/>
        <source>Performance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="174"/>
        <location filename="../widget/evaluationwidget.cpp" line="190"/>
        <location filename="../widget/evaluationwidget.cpp" line="201"/>
        <location filename="../widget/evaluationwidget.cpp" line="212"/>
        <location filename="../widget/evaluationwidget.cpp" line="228"/>
        <location filename="../widget/evaluationwidget.cpp" line="239"/>
        <location filename="../widget/evaluationwidget.cpp" line="250"/>
        <location filename="../widget/evaluationwidget.cpp" line="266"/>
        <location filename="../widget/evaluationwidget.cpp" line="277"/>
        <location filename="../widget/evaluationwidget.cpp" line="288"/>
        <location filename="../widget/evaluationwidget.cpp" line="304"/>
        <location filename="../widget/evaluationwidget.cpp" line="315"/>
        <location filename="../widget/evaluationwidget.cpp" line="326"/>
        <location filename="../widget/evaluationwidget.cpp" line="342"/>
        <location filename="../widget/evaluationwidget.cpp" line="353"/>
        <location filename="../widget/evaluationwidget.cpp" line="364"/>
        <location filename="../widget/evaluationwidget.cpp" line="380"/>
        <location filename="../widget/evaluationwidget.cpp" line="391"/>
        <source>Points</source>
        <translation>Points</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="178"/>
        <location filename="../widget/evaluationwidget.cpp" line="194"/>
        <location filename="../widget/evaluationwidget.cpp" line="205"/>
        <location filename="../widget/evaluationwidget.cpp" line="216"/>
        <location filename="../widget/evaluationwidget.cpp" line="232"/>
        <location filename="../widget/evaluationwidget.cpp" line="243"/>
        <location filename="../widget/evaluationwidget.cpp" line="254"/>
        <location filename="../widget/evaluationwidget.cpp" line="270"/>
        <location filename="../widget/evaluationwidget.cpp" line="281"/>
        <location filename="../widget/evaluationwidget.cpp" line="292"/>
        <location filename="../widget/evaluationwidget.cpp" line="308"/>
        <location filename="../widget/evaluationwidget.cpp" line="319"/>
        <location filename="../widget/evaluationwidget.cpp" line="330"/>
        <location filename="../widget/evaluationwidget.cpp" line="346"/>
        <location filename="../widget/evaluationwidget.cpp" line="357"/>
        <location filename="../widget/evaluationwidget.cpp" line="368"/>
        <location filename="../widget/evaluationwidget.cpp" line="384"/>
        <location filename="../widget/evaluationwidget.cpp" line="395"/>
        <source>%1 cpm and %2 errors in %3 minutes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="183"/>
        <source>No experience in touch typing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="221"/>
        <source>First steps in touch typing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="259"/>
        <source>Advanced level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="297"/>
        <source>Suitable skills</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="335"/>
        <source>Very good skills</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="373"/>
        <source>Perfect skills</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FingerWidget</name>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="233"/>
        <source>Error rates of your fingers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="240"/>
        <source>The error rate is based on the recorded characters and the current selected keyboard layout.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="251"/>
        <source>Error Rate:</source>
        <translation>Taux d&apos;erreur:</translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="253"/>
        <source>Frequency:</source>
        <translation>Fréquence:</translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="255"/>
        <source>Errors:</source>
        <translation>Erreurs :</translation>
    </message>
</context>
<context>
    <name>HelpBrowser</name>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="30"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="41"/>
        <location filename="../widget/helpbrowser.cpp" line="44"/>
        <source>en</source>
        <translation type="unfinished">fr</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="72"/>
        <source>Back</source>
        <translation>Retour</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="75"/>
        <source>Table of Contents</source>
        <translation>Sommaire</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="76"/>
        <source>&amp;Close</source>
        <translation>&amp;Fermer</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="79"/>
        <source>&amp;Print page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="118"/>
        <source>Print page</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>IllustrationDialog</name>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="29"/>
        <source>Introduction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="60"/>
        <source>Welcome to TIPP10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="65"/>
        <source>TIPP10 is a free touch typing tutor for Windows, Mac OS and Linux. The ingenious thing about the software is its intelligence feature. Characters that are mistyped are repeated more frequently. Touch typing has never been so easy to learn.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="72"/>
        <source>Tips for using the 10 finger system</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="76"/>
        <source>1. First place your fingers in the home position (this is displayed at the beginning of each lesson). The fingers return to the home row after each key is pressed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="82"/>
        <source>en</source>
        <translation type="unfinished">fr</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="86"/>
        <source>2. Make sure your posture is straight and avoid looking at the keyboard. Your eyes should be directed toward the monitor at all times.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="91"/>
        <source>3. Bring your arms to the side of your body and relax your shoulders. Your upper arm and lower arm should be at a right angle. Do not rest your wrists and remain in an upright position.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="97"/>
        <source>4. Try to remain relaxed during the typing lessons.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="100"/>
        <source>5. Try to keep typing errors to a minimum. It is much less efficient to type fast if you are making a lot of mistakes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="104"/>
        <source>6. Once you have begun touch typing you have to avoid reverting back to the way you used to type (even if you are in a hurry).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="108"/>
        <source>If you need assistance with using the software use the help function.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="119"/>
        <source>All rights reserved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="133"/>
        <source>&amp;Launch TIPP10</source>
        <translation>&amp;Démarrer TIPP10</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="138"/>
        <source>Do&amp;n&apos;t show me this window again</source>
        <translation>&amp;Ne plus afficher</translation>
    </message>
</context>
<context>
    <name>LessonDialog</name>
    <message>
        <location filename="../widget/lessondialog.cpp" line="102"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuler</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="103"/>
        <source>&amp;Save</source>
        <translation>&amp;Sauver</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="104"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="112"/>
        <source>Name of the lesson (20 characters max.):</source>
        <translation>Nom de la leçon (20 caractères max.) :</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="114"/>
        <source>Short description (120 characters max.):</source>
        <translation>Petite description (120 caractères max.) :</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="115"/>
        <source>Lesson content (at least two lines):</source>
        <translation>Contenu de la leçon (au moins 2 lignes):</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="117"/>
        <source>&lt;u&gt;Explanation:&lt;/u&gt;&lt;br&gt;&amp;nbsp;&lt;br&gt;Every line (separated by Enter key) is equivalent to a unit of the lesson. There are two types of lesson dictation:&lt;br&gt;&amp;nbsp;&lt;br&gt;&lt;b&gt;Sentence Lesson&lt;/b&gt; - every line (sentence) will be dictated exactly how it was entered here with a line break at the end.&lt;br&gt;&amp;nbsp;&lt;br&gt;&lt;b&gt;Word Lesson&lt;/b&gt; - the lines will be separated by blanks and a line break passes auomatically after at least %1 characters.</source>
        <translation>&lt;u&gt;Explication :&lt;/u&gt;&lt;br&gt;&amp;nbsp;&lt;br&gt;Chaque ligne est équivalente à une unité de leçon. Il y a deux façons de dicter une leçon :&lt;br&gt;&amp;nbsp;&lt;br&gt;&lt;b&gt;Leçon de phrases&lt;/b&gt; - chaque ligne (phrase) sera dictée jusqu&apos;à la fin.&lt;br&gt;&amp;nbsp;&lt;br&gt;&lt;b&gt;Leçon de mots&lt;/b&gt; - Les mots seront dictés les uns à la suite des autres mais contrairement au précédent mode, le retour à la ligne se fera après %1 caractères.</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="127"/>
        <source>&lt;b&gt;What happens when &quot;Intelligence&quot; is enabled?&lt;/b&gt;&lt;br&gt;With enabled intelligence, the lines to be dictated will be selected depending on the typing mistake quotas instead of dictating them in the right order. Enabling the &quot;Intelligence&quot; only makes sense if the lesson consists of many lines (often &quot;Word Lessons&quot;).&lt;br&gt;</source>
        <translation>&lt;b&gt;Que se passe-t-il quand le mode Intelligent est actif ?&lt;/b&gt;&lt;br&gt;Les lignes dictées seront sélectionnées en fonction de vos erreurs de frappe. Activer ce mode est possible uniquement pour les leçons comportant beaucoup de lignes (souvent le cas des leçons de mots).&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="135"/>
        <source>Dictate the text as:</source>
        <translation>Type de leçon :</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="146"/>
        <source>Sentence Lesson</source>
        <translation>Leçon de phrases</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="148"/>
        <source>Every line will be completed with
a line break at the end</source>
        <translation>Chaque ligne sera dictée
avec un retour chariot à la fin</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="151"/>
        <source>Word Lesson</source>
        <translation>Leçon de mots</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="153"/>
        <source>Every unit (line) will be separated
by blanks. A line break passes
automatically after at least %1 characters.</source>
        <translation>Every unit (line) will be seperated 
by blanks. A line break passes 
auomatically after at least %1 caractères.</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="160"/>
        <source>Edit own Lesson</source>
        <translation>Modifier la leçon personnelle</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="162"/>
        <source>Name of the Lesson:</source>
        <translation>Nom de la leçon :</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="166"/>
        <source>Create own Lesson</source>
        <translation>Créer une leçon personnelle</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="226"/>
        <source>Please enter the name of the lesson
</source>
        <translation>Veuillez entrer le nom de la leçon
</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="232"/>
        <source>Please enter entire lesson content
</source>
        <translation>Veuillez entrer le contenu de la leçon
</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="249"/>
        <source>Please enter at least two lines of text
</source>
        <translation>Veuillez entrer au moins deux lignes
</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="255"/>
        <source>Please enter 400 lines of text maximum
</source>
        <translation>Veuillez entrer 400 lignes maximum
</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="263"/>
        <source>The name of the lesson already exists. Please enter a new lesson name.
</source>
        <translation>Ce nom de leçon existe déjà. Veuillez en saisir un autre.
</translation>
    </message>
</context>
<context>
    <name>LessonPrintDialog</name>
    <message>
        <location filename="../widget/lessonprintdialog.cpp" line="25"/>
        <source>Print Lesson</source>
        <translation>Imprimer la leçon</translation>
    </message>
    <message>
        <location filename="../widget/lessonprintdialog.cpp" line="45"/>
        <source>&amp;Print</source>
        <translation>&amp;Imprimer</translation>
    </message>
    <message>
        <location filename="../widget/lessonprintdialog.cpp" line="46"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuler</translation>
    </message>
    <message>
        <location filename="../widget/lessonprintdialog.cpp" line="60"/>
        <source>Please enter your name:</source>
        <translation>Veuillez entre votre nom :</translation>
    </message>
</context>
<context>
    <name>LessonResult</name>
    <message>
        <location filename="../widget/lessonresult.cpp" line="51"/>
        <source>Print</source>
        <translation>Imprimer</translation>
    </message>
    <message numerus="yes">
        <location filename="../widget/lessonresult.cpp" line="73"/>
        <source>%n minute(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../widget/lessonresult.cpp" line="78"/>
        <source>%n character(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="83"/>
        <source>Entire Lesson</source>
        <translation>Leçon complète</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="90"/>
        <source>Error Correction with Backspace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="93"/>
        <source>Error Correction without Backspace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="95"/>
        <source>Ignore Errors</source>
        <translation>Ignorer les erreurs</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="103"/>
        <source>None</source>
        <translation>Aucune</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="111"/>
        <source>All</source>
        <translation>Tous</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="119"/>
        <source>- Colored Keys</source>
        <translation>- touches colorées</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="125"/>
        <source>- Home Row</source>
        <translation>- indication des doigts sur la rangée du milieu</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="131"/>
        <source>- Motion Paths</source>
        <translation>- trait de déplacement des doigts</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="137"/>
        <source>- Separation Line</source>
        <translation>- ligne de séparation</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="143"/>
        <source>- Instructions</source>
        <translation>- indication des doigts à utiliser</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="175"/>
        <source>%L1 s</source>
        <translation>%L1 s</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="178"/>
        <source>%L1 min</source>
        <translation>%L1 min</translation>
    </message>
    <message numerus="yes">
        <location filename="../widget/lessonresult.cpp" line="188"/>
        <source>%n point(s)</source>
        <translation type="unfinished">
            <numerusform>%n point</numerusform>
            <numerusform>%n points</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../widget/lessonresult.cpp" line="254"/>
        <location filename="../widget/lessonresult.cpp" line="449"/>
        <source>You have reached %1 at a typing speed of %2 cpm and %n typing error(s).</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="272"/>
        <location filename="../widget/lessonresult.cpp" line="467"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="283"/>
        <location filename="../widget/lessonresult.cpp" line="330"/>
        <location filename="../widget/lessonresult.cpp" line="479"/>
        <location filename="../widget/lessonresult.cpp" line="525"/>
        <source>Duration: </source>
        <translation>Durée : </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="287"/>
        <location filename="../widget/lessonresult.cpp" line="483"/>
        <source>Typing Errors: </source>
        <translation>Erreurs de frappe : </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="291"/>
        <location filename="../widget/lessonresult.cpp" line="487"/>
        <source>Assistance: </source>
        <translation>Assistance: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="305"/>
        <location filename="../widget/lessonresult.cpp" line="500"/>
        <source>Results</source>
        <translation>Résultats</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="322"/>
        <location filename="../widget/lessonresult.cpp" line="517"/>
        <source>Lesson: </source>
        <translation>Leçon: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="326"/>
        <location filename="../widget/lessonresult.cpp" line="521"/>
        <source>Time: </source>
        <translation>Temps : </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="334"/>
        <location filename="../widget/lessonresult.cpp" line="529"/>
        <source>Characters: </source>
        <translation>Caractères : </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="338"/>
        <location filename="../widget/lessonresult.cpp" line="533"/>
        <source>Errors: </source>
        <translation>Erreurs : </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="342"/>
        <location filename="../widget/lessonresult.cpp" line="537"/>
        <source>Error Rate: </source>
        <translation>Taux d&apos;erreur: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="346"/>
        <location filename="../widget/lessonresult.cpp" line="541"/>
        <source>Cpm: </source>
        <translation>CPM : </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="360"/>
        <location filename="../widget/lessonresult.cpp" line="554"/>
        <source>Dictation</source>
        <translation>Dictée</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="443"/>
        <source>TIPP10 Touch Typing Tutor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="446"/>
        <source>Report</source>
        <translation>Rapport</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="446"/>
        <source> of %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="571"/>
        <source>Print Report</source>
        <translation>Imprimer le rapport</translation>
    </message>
</context>
<context>
    <name>LessonSqlModel</name>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="69"/>
        <source>%L1 s</source>
        <translation>%L1 s</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="73"/>
        <source>%L1 min</source>
        <translation>%L1 min</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="77"/>
        <source>%L1 %</source>
        <translation>%L1 %</translation>
    </message>
    <message numerus="yes">
        <location filename="../sql/lessontablesql.cpp" line="89"/>
        <source>%n point(s)</source>
        <translation>
            <numerusform>%n point</numerusform>
            <numerusform>%n points</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>LessonTableSql</name>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="126"/>
        <source>Show: </source>
        <translation>Afficher : </translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="128"/>
        <source>All Lessons</source>
        <translation>Toutes les leçons</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="129"/>
        <source>Training Lessons</source>
        <translation>Leçons d&apos;entraînement</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="130"/>
        <source>Open Lessons</source>
        <translation>Leçons ouvertes</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="131"/>
        <source>Own Lessons</source>
        <translation>Leçons personnelles</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="266"/>
        <source>Lesson</source>
        <translation>Leçon</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="267"/>
        <source>Time</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="268"/>
        <source>Duration</source>
        <translation>Durée</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="269"/>
        <source>Characters</source>
        <translation>Caractères</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="270"/>
        <source>Errors</source>
        <translation>Erreurs</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="271"/>
        <source>Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="272"/>
        <source>Cpm</source>
        <translation>CPM</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="273"/>
        <source>Score</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="276"/>
        <source>This column shows the names
of completed lessons</source>
        <translation>Cette colonne affiche le nom
des leçons complétées</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="280"/>
        <source>Start time of the lesson</source>
        <translation>Début de la leçon</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="282"/>
        <source>Total duration of the lesson</source>
        <translation>Durée totale de la leçon</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="283"/>
        <source>Number of characters dictated</source>
        <translation>Nombre de caractères dictés</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="286"/>
        <source>Number of typing errors</source>
        <translation>Nombre d&apos;erreurs de frappe</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="288"/>
        <source>The error rate is calculated as follows:
Errors / Characters
The lower the error rate the better!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="293"/>
        <source>&quot;Cpm&quot; indicates how many characters per minute
were entered on average</source>
        <translation>&quot;CPM&quot; indique le nombre moyen de caractères tapés par minute</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="297"/>
        <source>The score is calculated as follows:
((Characters - (20 x Errors)) / Duration in minutes) x 0.4

Note that slow typing without errors results in a
better ranking, than fast typing with several errors!</source>
        <translation>Votre classement est calculé ainsi :
((caractères - (20 x erreurs) / durée en minutes ) x 0.4

Notez que tapez lentement sans erreur permet un meilleur classement
qu&apos;une frappe rapide avec beaucoup d&apos;erreurs !</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../widget/mainwindow.cpp" line="61"/>
        <source>All results of the current lesson will be discarded!

Do you really want to exit?

</source>
        <translation>Tous les résultats de la leçon courante seront perdus

Voulez-vous vraiment quitter ?

</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="82"/>
        <location filename="../widget/mainwindow.cpp" line="100"/>
        <source>&amp;Go</source>
        <translation>&amp;Aller à</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="88"/>
        <location filename="../widget/mainwindow.cpp" line="104"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="96"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="115"/>
        <source>&amp;Settings</source>
        <translation>&amp;Paramètres</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="116"/>
        <source>E&amp;xit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="118"/>
        <source>&amp;Results</source>
        <translation>&amp;Résultats</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="119"/>
        <source>&amp;Manual</source>
        <translation>&amp;Manuel</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="121"/>
        <source> on the web</source>
        <translation> sur Internet</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="123"/>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="125"/>
        <source>&amp;About </source>
        <translation>&amp;A propos </translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="128"/>
        <source>ABC-Game</source>
        <translation>Jeu ABC</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="182"/>
        <source>Software Version </source>
        <translation>Version </translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="183"/>
        <source>Database Version </source>
        <translation>Version de la base de données </translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="188"/>
        <source>Intelligent Touch Typing Tutor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="189"/>
        <source>On the web: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="191"/>
        <source>TIPP10 is published under the GNU General Public License and is available for free. You do not have to pay for it wherever you download it!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="201"/>
        <source>About </source>
        <translation>A propos </translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="185"/>
        <source>Portable Version</source>
        <translation>Version portable</translation>
    </message>
</context>
<context>
    <name>ProgressionWidget</name>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="39"/>
        <location filename="../widget/progressionwidget.cpp" line="50"/>
        <location filename="../widget/progressionwidget.cpp" line="504"/>
        <source>Time</source>
        <translation>Temps</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="42"/>
        <source>Show: </source>
        <translation>Afficher : </translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="44"/>
        <source>All Lessons</source>
        <translation>Toutes les leçons</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="45"/>
        <source>Training Lessons</source>
        <translation>Exercices</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="46"/>
        <source>Open Lessons</source>
        <translation>Leçons libres</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="47"/>
        <source>Own Lessons</source>
        <translation>Leçons personnelles</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="48"/>
        <source>Order by x-axis:</source>
        <translation>Ordre des abscisses :</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="51"/>
        <location filename="../widget/progressionwidget.cpp" line="510"/>
        <source>Lesson</source>
        <translation>Leçon</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="229"/>
        <source>Points</source>
        <translation>Points</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="360"/>
        <location filename="../widget/progressionwidget.cpp" line="384"/>
        <source>Training Lesson</source>
        <translation>Exercice</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="367"/>
        <location filename="../widget/progressionwidget.cpp" line="391"/>
        <source>Open Lesson</source>
        <translation>Leçon libre</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="374"/>
        <location filename="../widget/progressionwidget.cpp" line="398"/>
        <source>Own Lesson</source>
        <translation>Leçon personnelle</translation>
    </message>
    <message numerus="yes">
        <location filename="../widget/progressionwidget.cpp" line="455"/>
        <source>%n point(s)</source>
        <translation type="unfinished">
            <numerusform>%n point</numerusform>
            <numerusform>%n points</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="456"/>
        <source>%1 cpm</source>
        <translation>%1 CPM</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="473"/>
        <source>The progress graph will be shown after completing two lessons.</source>
        <translation>Les statistiques de progression seront affichées après avoir effectué deux leçons.</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="515"/>
        <source>Cpm</source>
        <translation>CPM</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="520"/>
        <source>Score</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../sql/connection.h" line="97"/>
        <location filename="../sql/connection.h" line="105"/>
        <location filename="../sql/connection.h" line="125"/>
        <source>Affected directory:
</source>
        <translation>Chemin :
</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="31"/>
        <location filename="../widget/fingerwidget.cpp" line="120"/>
        <source>Left little finger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="32"/>
        <location filename="../widget/fingerwidget.cpp" line="121"/>
        <source>Left ring finger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="33"/>
        <location filename="../widget/fingerwidget.cpp" line="122"/>
        <source>Left middle finger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="34"/>
        <location filename="../widget/fingerwidget.cpp" line="123"/>
        <source>Left forefinger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="34"/>
        <location filename="../widget/fingerwidget.cpp" line="124"/>
        <source>Right forefinger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="35"/>
        <location filename="../widget/fingerwidget.cpp" line="125"/>
        <source>Right middle finger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="36"/>
        <location filename="../widget/fingerwidget.cpp" line="126"/>
        <source>Right ring finger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="37"/>
        <location filename="../widget/fingerwidget.cpp" line="127"/>
        <source>Right little finger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="37"/>
        <location filename="../widget/fingerwidget.cpp" line="127"/>
        <source>Thumb</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="88"/>
        <source>en</source>
        <translation>fr</translation>
    </message>
</context>
<context>
    <name>RegExpDialog</name>
    <message>
        <location filename="../widget/regexpdialog.ui" line="14"/>
        <source>Filter for the keyboard layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/regexpdialog.ui" line="20"/>
        <source>Limitation of characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/regexpdialog.ui" line="36"/>
        <source>You should try to avoid using characters not supported by your keyboard layout. A filter as a regular expression is applied to all practice texts before the lesson begins. You should only make changes here if you are familiar with regular expressions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/regexpdialog.ui" line="46"/>
        <source>Replacement Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/regexpdialog.ui" line="61"/>
        <source>Filtering unauthorized characters can produce texts that make little sense (e.g., by removing umlauts). You can define replacements that will be applied before the limitation of characters is applied. Please follow the example here that replaces all Germans umlauts and the ß symbol:
ae=ae,oe=oe,ue=ue,Ae=Ae,Oe=Oe,Ue=Ue,ss=ss</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../widget/settings.cpp" line="74"/>
        <source>Some of your settings require a restart of the software to take effect.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.cpp" line="291"/>
        <source>All data for completed lessons for the current user will be deleted
and the lesson list will return to its original state after initial installation!

Do you really want to continue?

</source>
        <translation>Toutes les indicateurs d&apos;achèvement des leçons vont être supprimés
ainsi que l&apos;historique des leçons effectuées!

Voulez-vous continuer?

</translation>
    </message>
    <message>
        <location filename="../widget/settings.cpp" line="305"/>
        <source>The data for completed lessons was successfully deleted!
</source>
        <translation>Les indicateurs d&apos;achèvement des leçons ont été supprimées avec succès!
</translation>
    </message>
    <message>
        <location filename="../widget/settings.cpp" line="316"/>
        <source>All recorded characters (mistake quotas) of the current user will be deleted and the character list will return to its original state after initial installation!

Do you really want to continue?</source>
        <translation>Toutes les statistiques des caractères tapés vont être supprimés!

Voulez-vous continuer?</translation>
    </message>
    <message>
        <location filename="../widget/settings.cpp" line="330"/>
        <source>The recorded characters were successfully deleted!
</source>
        <translation>Les statistiques des caractères tapés ont été supprimées avec succès!
</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="20"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="42"/>
        <source>Training</source>
        <translation>Entraînement</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="48"/>
        <source>Ticker</source>
        <translation>Téléscripteur</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="62"/>
        <source>Here you can select the background color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="78"/>
        <source>Here you can select the font color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="88"/>
        <source>Cursor:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="101"/>
        <source>Here you can change the font of the ticker (a font size larger than 20 pt is not recommended)</source>
        <translation>Ici vous pouvez changer la police de caractères du téléscripteur
(une taille supérieure à 20 points n&apos;est pas recommandée)</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="104"/>
        <source>Change &amp;Font</source>
        <translation>&amp;Changer la police</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="117"/>
        <source>Here can select the color of the cursor for the current character</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="133"/>
        <source>Font:</source>
        <translation>Police:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="140"/>
        <source>Font Color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="147"/>
        <source>Background:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="164"/>
        <source>Speed:</source>
        <translation>Vitesse:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="179"/>
        <source>Off</source>
        <translation>Lente</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="192"/>
        <source>Here you can change the speed of the ticker (Slider on the left: Ticker does not move until reaching the end of line. Slider on the right: The ticker moves very fast.)</source>
        <translation>Ici vous pouvez régler la vitesse du téléscripteur
(curseur à gauche: le téléscripteur ne défilera pas jusqu&apos;à la fin de la ligne.
curseur à droite: le téléscripteur dévilera très rapidement.)</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="214"/>
        <source>Fast</source>
        <translation>Rapide</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="228"/>
        <source>Audio Output</source>
        <translation>Sortie audio</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="234"/>
        <source>Here you can activate a metronome</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="237"/>
        <source>Metronome:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="266"/>
        <source>Select how often the metronome sound should appear per minute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="272"/>
        <source> cpm</source>
        <translation> CPM</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="292"/>
        <location filename="../widget/settings.ui" line="298"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="317"/>
        <source>Advanced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="324"/>
        <source>Training Lessons:</source>
        <translation>Exercices:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="342"/>
        <source>The training lessons you have chosen are not suited for the keyboard layout. You can continue but you may have to put aside some keys from the beginning.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="352"/>
        <source>Keyboard Layout:</source>
        <translation>Disposition de clavier:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="376"/>
        <source>Results</source>
        <translation>Résultats</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="382"/>
        <source>User Data</source>
        <translation>Données utilisateur</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="388"/>
        <source>Here you can reset all saved lesson data (the lessons will be empty as they were after initial installation</source>
        <translation>Ici vous pouvez effacer les indicateurs d&apos;achèvement des leçons ainsi que l&apos;historique des leçons effectuées</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="391"/>
        <source>Reset &amp;completed lessons</source>
        <translation>Effacer les &amp;indicateurs de leçons achevées</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="398"/>
        <source>Here you can reset all recorded keystrokes and typing mistakes (the characters will be empty as they were after initial installation)</source>
        <translation>Ici vous pouvez effacer les statistiques des caractères tapés</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="401"/>
        <source>Reset all &amp;recorded characters</source>
        <translation>Effacer les statistiques des &amp;caractères tapés</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="412"/>
        <source>Other</source>
        <translation>Autre</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="418"/>
        <source>Windows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="424"/>
        <source>Here you can decide if an information window with tips is shown at the beginning</source>
        <translation>Ici vous pouvez choisir d&apos;afficher une fenêtre d&apos;informations
au démarrage</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="427"/>
        <source>Show welcome message at startup</source>
        <translation>Afficher le message de bienvenue au démarrage</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="434"/>
        <source>Here you can define whether a warning window is displayed when an open or own lesson with active intelligence is started</source>
        <translation>Ici vous pouvez choisir d&apos;afficher un avertissement
lorsqu&apos;une leçon en mode intelligent commence</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="437"/>
        <source>Remember enabled intelligence before starting
an open or own lesson</source>
        <translation>Sauver le mode intelligent avant
de commencer une leçon</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="451"/>
        <source>Change duration of the lesson automatically to
&quot;Entire lesson&quot; when disabling the intelligence</source>
        <translation>Sélectionner la durée complète de la leçon
à la désactivation du mode intelligent</translation>
    </message>
</context>
<context>
    <name>StartWidget</name>
    <message>
        <location filename="../widget/startwidget.cpp" line="87"/>
        <location filename="../widget/startwidget.cpp" line="731"/>
        <source>Training Lessons</source>
        <translation>Leçons d&apos;entraînement</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="94"/>
        <source>Subject:</source>
        <translation>Thème:</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="146"/>
        <source>&amp;Edit</source>
        <translation>&amp;Editer</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="150"/>
        <source>&amp;New Lesson</source>
        <translation>&amp;Nouvelle leçon</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="152"/>
        <source>&amp;Import Lesson</source>
        <translation>&amp;Importer la leçon</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="154"/>
        <source>&amp;Export Lesson</source>
        <translation>&amp;Exporter la leçon</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="156"/>
        <source>&amp;Edit Lesson</source>
        <translation>&amp;Editer la leçon</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="158"/>
        <source>&amp;Delete Lesson</source>
        <translation>&amp;Supprimer la leçon</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="187"/>
        <source>Duration of Lesson</source>
        <translation>Durée de la leçon</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="190"/>
        <source>Time Limit:</source>
        <translation>Limite de temps:</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="192"/>
        <location filename="../widget/startwidget.cpp" line="199"/>
        <source>The dictation will be stopped after
a specified time period</source>
        <translation>La dictée sera arrêtée après
un temps donné</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="198"/>
        <source> minutes</source>
        <translation> minutes</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="203"/>
        <source>Character Limit:</source>
        <translation>Limite des caractères:</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="213"/>
        <source> characters</source>
        <translation> caractères</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="205"/>
        <location filename="../widget/startwidget.cpp" line="215"/>
        <source>The dictation will be stopped after
a specified number of correctly typed
characters</source>
        <translation>La dictée sera arrêté après
un certain nombre de caractères
tapés</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="219"/>
        <source>Entire
Lesson</source>
        <translation>Leçon complète</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="221"/>
        <source>The complete lesson will be dictated
from beginning to end</source>
        <translation>La leçon sera dictée dans
sa globalité</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="222"/>
        <source>(Entire Lesson)</source>
        <translation>(leçon entière)</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="252"/>
        <source>Response to Typing Errors</source>
        <translation>Prise en charge des erreurs de frappe</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="255"/>
        <source>Block Typing Errors</source>
        <translation>Arrêter le téléscripteur</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="257"/>
        <source>The dictation will only proceed if the correct
key was pressed</source>
        <translation>La dictée continuera uniquement si
la bonne touche est pressée</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="260"/>
        <source>Correction with Backspace</source>
        <translation>Correction des erreurs</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="262"/>
        <source>Typing errors have to be removed
with the return key</source>
        <translation>Les erreurs de frappe doivent être
corrigées avec la touche Backspace</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="265"/>
        <source>Audible Signal</source>
        <translation>Signal acoustique</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="266"/>
        <source>A beep sounds with every typing error</source>
        <translation>Emettre un bip à chaque erreur de frappe</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="269"/>
        <source>Show key picture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="270"/>
        <source>For every typing error the corresponding key picture is displayed on the keyboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="276"/>
        <source>*The text of the lesson will not be dictated in its intended sequence, but will be adjusted in real time to your typing errors.</source>
        <translation>* Le texte de la leçon sera dicté en fonction de vos erreurs de frappe en temps réel.</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="282"/>
        <source>*Select this option if the text of the lesson will not be dictated in its intended sequence, but will be adjusted in real time to your typing errors.</source>
        <translation>* Sélectionnez cette option afin d&apos;adapter l&apos;ordre de diction des mots de la dictée à vos erreurs en temps réel.</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="289"/>
        <source>Intelligence</source>
        <translation>Mode intelligent</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="291"/>
        <source>Based on the current error rates of all characters, the wordsand phrases of the dictation will be selected in real time.On the other hand, if the intelligence box is not checked, the text ofthe lesson is always dictated in the same order.</source>
        <translation>Les mots et les phrases de la leçon seront dictés 
en fonction de vos erreurs de frappe en temps réel.
Cela est valable uniquement si le mode intelligent est sélectionné.</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="323"/>
        <source>Assistance</source>
        <translation>Assistance</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="325"/>
        <source>Show Keyboard</source>
        <translation>Afficher le clavier virtuel</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="326"/>
        <source>For visual support, the virtual keyboard and
status information is shown</source>
        <translation>Pour un support visuel, le clavier virtuel et
les informations de statut seront affichées</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="330"/>
        <source>Colored Keys</source>
        <translation>Touches Colorées</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="332"/>
        <source>For visual support pressing keys will be
marked with colors</source>
        <translation>Pour un support visuel les touches 
à taper seront colorées</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="335"/>
        <source>Home Row</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="337"/>
        <source>For visual support, the remaining fingers
of the home row will be colored</source>
        <translation>Pour un support visuel les doigts de la rangée
du milieu seront colorés</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="341"/>
        <source>L/R Separation Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="343"/>
        <source>For visual support a separation line between left
and right hand will be shown</source>
        <translation>Pour un support visuel une ligne de séparation
entre la main droite et la main gauche sera affichée</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="347"/>
        <source>Instruction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="349"/>
        <source>Show fingers to be used in the status bar</source>
        <translation>Les doigts à utiliser seront affichés dans la barre de statut</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="352"/>
        <source>Motion Paths</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="354"/>
        <source>Motion paths of the fingers will be shown
on the keyboard</source>
        <translation>Les déplacements de doigts seront affichés
sur le clavier</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="401"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="402"/>
        <source>&amp;Start Training</source>
        <translation>&amp;Démarrer la leçon</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="523"/>
        <source>All</source>
        <translation>Tous</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="743"/>
        <source>Open Lessons</source>
        <translation>Leçons ouvertes</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="773"/>
        <source>At the moment open lessons only exists in German language. We hope to provide open lessons in English soon.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="782"/>
        <source>Own Lessons</source>
        <translation>Leçons personnelles</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="906"/>
        <source>Please select a text file</source>
        <translation>Veuillez sélectionner un fichier texte</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="907"/>
        <source>Text files (*.txt)</source>
        <translation>Fichiers textes (*.txt)</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="1029"/>
        <source>Please indicate the location of a text file</source>
        <translation>Veuillez indiquer l&apos;emplacement d&apos;un fichier texte</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="1078"/>
        <source>Do you really want to delete the lesson, and all the recorded data in the context of this lesson?</source>
        <translation>Voulez-vous vraiment supprimer la leçon
and toutes les statistiques enregistrées pour
celle-ci?</translation>
    </message>
</context>
<context>
    <name>TickerBoard</name>
    <message>
        <location filename="../widget/tickerboard.h" line="120"/>
        <source>Press space bar to proceed</source>
        <translation>Tapez Espace pour démarrer</translation>
    </message>
    <message>
        <location filename="../widget/tickerboard.cpp" line="137"/>
        <source>Dictation Finished</source>
        <translation>Dictée terminée</translation>
    </message>
</context>
<context>
    <name>TrainingWidget</name>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="99"/>
        <source>&amp;Pause</source>
        <translatorcomment>&amp;Pause</translatorcomment>
        <translation>&amp;Pause</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="106"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuler</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="112"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="214"/>
        <source>Press space bar to start</source>
        <translation>Appuyez sur la touche Espace pour commencer</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="215"/>
        <source>Take Home Row position</source>
        <translation>Position du mileu</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="231"/>
        <source>Do you really want to exit the lesson early?</source>
        <translation>Voulez-vous vraiment quitter la leçon ?</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="245"/>
        <source>Do you want to save your results?</source>
        <translation>Voulez-vous sauver vos résultats ?</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="462"/>
        <source>E&amp;xit Lesson early</source>
        <translation>&amp;Quitter la leçon</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="515"/>
        <source>Errors: </source>
        <translation>Erreurs : </translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="517"/>
        <source>Cpm: </source>
        <translation>CPM : </translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="518"/>
        <source>Time: </source>
        <translation>Temps : </translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="520"/>
        <source>Characters: </source>
        <translation>Caractères : </translation>
    </message>
</context>
</TS>
